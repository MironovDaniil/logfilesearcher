package com.GUI;

import com.fileInterraction.DirectoryHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.StageStyle;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;

public class MainController {
    @FXML
    private TreeView<File> treeView;
    @FXML
    private TabPane tabPane;

    private Map<File, List<Long>> validFiles;
    private File root;
    private String extension;
    private String searchedText;

    private MainApp mainApp;

    public MainController() {
        extension = "log";
        searchedText = "";
        root = null;
        validFiles = null;
    }

    @FXML
    private void initialize() {
    }

    @FXML
    private void handleSetExtension() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Ввод");
        dialog.setHeaderText("Ввод расширения");
        dialog.setContentText("Введите искомое расширение файла:");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(this::setExtension);
    }

    @FXML
    private void handleSetDirectory() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Ввод");
        dialog.setHeaderText("Ввод директории");
        dialog.setContentText("Введите полный путь до директории поиска:");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            File file = new File(result.get());
            if (!file.exists()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Ошибка");
                alert.setHeaderText("Нельзя выбрать эту директорию!");
                alert.setContentText("Данная директория не существует!");

                alert.showAndWait();
            } else if (!file.isDirectory()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Ошибка");
                alert.setHeaderText("Нельзя выбрать эту директорию!");
                alert.setContentText("Полученный файл не является директорией!");

                alert.showAndWait();
            } else {
                setRoot(file);
            }
        }
    }

    @FXML
    private void handleSearch() {
        if (root == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initStyle(StageStyle.UTILITY);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Невозможно искать!");
            alert.setContentText("Сначала укажите директорию!");

            alert.showAndWait();
            return;
        }
        TextInputDialog dialog = new TextInputDialog("");
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Ввод");
        dialog.setHeaderText("Ввод текста");
        dialog.setContentText("Введите искомый текст:");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(this::setSearchedText);
        if ((searchedText.equals("")) || (searchedText.equals(" "))) {
            return;
        }
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Callable task = () -> {
            DirectoryHandler directoryHandler = new DirectoryHandler(root, extension);
            try {
                validFiles = directoryHandler.searchFiles(searchedText);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        };
        Future future = executor.submit(task);
        try {
            future.get();
            drawTree();
            executor.shutdown();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void drawTree(@NotNull TreeItem<File> root) {
        File folder = root.getValue();
        File[] folderEntries = folder.listFiles();
        if (folderEntries == null) {
            return;
        }
        for (File entry : folderEntries
        ) {
            if (entry.isDirectory()) {
                Image icon = new Image(getClass().getResourceAsStream("/Folder.png"));
                TreeItem<File> added = new TreeItem<>(new extFile(entry.getAbsolutePath()), new ImageView(icon));
                root.getChildren().add(added);
                drawTree(added);
            } else if (validFiles.containsKey(entry)) {
                Image icon = new Image(getClass().getResourceAsStream("/file.png"));
                TreeItem<File> added = new TreeItem<>(new extFile(entry.getAbsolutePath()), new ImageView(icon));
                root.getChildren().add(added);
            }
        }
    }

    @FXML
    private void drawTree() {
        if (root == null) {
            return;
        }
        Image icon = new Image(getClass().getResourceAsStream("/Folder.png"));
        TreeItem<File> treeRoot = new TreeItem<>(new extFile(root.getAbsolutePath()), new ImageView(icon));
        treeView.setRoot(treeRoot);
        drawTree(treeRoot);
    }

    @FXML
    private void handleDoubleClick(@NotNull MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) {
            File file = treeView.getSelectionModel().getSelectedItem().getValue();
            if (file.isDirectory()) {
                treeView.getSelectionModel().getSelectedItem().setExpanded(true);
            } else {
                List<Long> linesIndex = validFiles.get(file);
                try {
                    addTab(file, linesIndex);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void addTab(File file, List<Long> linesIndex) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/FileTab.fxml"));
        Tab tab = loader.load();

        TabController controller = loader.getController();
        controller.setFile(file);
        controller.setLinesIndex(linesIndex);
        tab.setText(file.getName());
        tab.setClosable(true);
        tabPane.getTabs().add(tab);
        controller.show();
    }

    public MainApp getMainApp() {
        return mainApp;
    }

    public File getRoot() {
        return root;
    }

    public String getExtension() {
        return extension;
    }

    public String getSearchedText() {
        return searchedText;
    }

    public Map<File, List<Long>> getValidFiles() {
        return validFiles;
    }

    void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    private void setRoot(@NotNull File root) {
        this.root = root;
    }

    private void setExtension(String extension) {
        this.extension = extension;
    }

    private void setSearchedText(String searchedText) {
        this.searchedText = searchedText;
    }

    public void setValidFiles(Map<File, List<Long>> validFiles) {
        this.validFiles = validFiles;
    }
}

class extFile extends File {
    extFile(String pathname) {
        super(pathname);
    }

    @Override
    public String toString() {
        return getName();
    }
}
