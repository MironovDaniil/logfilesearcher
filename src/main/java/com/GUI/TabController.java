package com.GUI;

import com.fileInterraction.FileViewer;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.stage.StageStyle;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.List;
import java.util.concurrent.*;

public class TabController {
    @FXML
    private TextArea textArea;

    private File file;
    private List<Long> linesIndex;
    private int currentLineIndex;
    private int currentSideLinesAfter;
    private int currentSideLinesBefore;
    private long linesCount;

    public TabController() {
        file = null;
        linesIndex = null;
        currentSideLinesAfter = 0;
        currentSideLinesBefore = 0;
        currentLineIndex = 0;
        linesCount = 0;
    }

    @FXML
    private void initialize() {

    }

    @FXML
    void show() {
        if ((file == null) || (linesIndex == null)) {
            textArea.setText("");
            return;
        }
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Callable task = () -> {
            FileViewer fileViewer = new FileViewer(file, linesCount);
            try {
                return fileViewer.viewString(
                        linesIndex.get(currentLineIndex),
                        currentSideLinesBefore,
                        currentSideLinesAfter);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        };
        Future future = executor.submit(task);
        try {
            textArea.setText((String) future.get());
            executor.shutdown();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleMoreLines() {
        if ((file == null) || (linesIndex == null)) {
            textArea.setText("");
            return;
        }
        if ((currentSideLinesAfter + linesIndex.get(currentLineIndex) == linesCount)
                && ((linesIndex.get(currentLineIndex)) - currentSideLinesBefore - 1 == 0)) {
        } else if (currentSideLinesAfter + linesIndex.get(currentLineIndex) == linesCount) {
            currentSideLinesBefore++;
            show();
        } else if ((linesIndex.get(currentLineIndex)) - currentSideLinesBefore - 1 == 0) {
            currentSideLinesAfter++;
            show();
        } else {
            currentSideLinesAfter++;
            currentSideLinesBefore++;
            show();
        }
    }

    @FXML
    private void handleLessLines() {
        if ((file == null) || (linesIndex == null)) {
            textArea.setText("");
            return;
        }
        if ((currentSideLinesBefore != 0) && (currentSideLinesAfter != 0)) {
            currentSideLinesBefore--;
            currentSideLinesAfter--;
            show();
        } else if (currentSideLinesBefore != 0) {
            currentSideLinesBefore--;
            show();
        } else if (currentSideLinesAfter != 0) {
            currentSideLinesAfter--;
            show();
        }
    }

    @FXML
    private void handleNext() {
        if ((file == null) || (linesIndex == null)) {
            textArea.setText("");
            return;
        }
        if (currentLineIndex < linesIndex.size() - 1) {
            currentLineIndex++;
            show();
        } else {
            currentLineIndex = 0;
            show();
        }
    }

    @FXML
    private void handlePrevious() {
        if ((file == null) || (linesIndex == null)) {
            textArea.setText("");
            return;
        }
        if (currentLineIndex > 0) {
            currentLineIndex--;
        } else {
            currentLineIndex = linesIndex.size() - 1;
        }
    }

    @FXML
    private void handleSelectAll() {
        if ((file == null) || (linesIndex == null)) {
            textArea.setText("");
            return;
        }
        textArea.selectAll();
    }

    private long countLines() throws IOException {
        try (InputStream is = new BufferedInputStream(new FileInputStream(file.getAbsolutePath()))) {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        }
    }

    public File getFile() {
        return file;
    }

    public List<Long> getLinesIndex() {
        return linesIndex;
    }

    void setFile(@NotNull File file) {
        this.file = file;
        try {
            linesCount = countLines();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void setLinesIndex(List<Long> linesIndex) {
        this.linesIndex = linesIndex;
    }
}
