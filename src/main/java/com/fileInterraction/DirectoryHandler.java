package com.fileInterraction;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DirectoryHandler {
    private File directory;
    private String extension;

    public DirectoryHandler(File directory, @NotNull String extension) {
        this.directory = directory;
        this.extension = extension.toUpperCase();
    }

    @Nullable
    private Map<File, List<Long>> searchFiles(String searchedText, @NotNull File directory) throws FileNotFoundException {
        if (!directory.isDirectory()) {
            return null;
        }
        Map<File, List<Long>> result = new HashMap<>();
        File[] folderEntries = directory.listFiles();
        if (folderEntries == null) {
            return null;
        }
        if (folderEntries.length == 0) {
            return result;
        }
        for (File entry : folderEntries
        ) {
            if (!entry.isDirectory()) {
                if (entry.getName().toUpperCase().endsWith(extension)) {
                    FileHandler handler = new FileHandler(entry);
                    List<Long> searchResult = handler.search(searchedText);
                    if (searchResult.size() != 0) {
                        result.put(entry, searchResult);
                    }
                }
            } else {
                result.putAll(Objects.requireNonNull(searchFiles(searchedText, entry)));
            }
        }
        return result;
    }

    public Map<File, List<Long>> searchFiles(String searchedText) throws FileNotFoundException {
        return searchFiles(searchedText, directory);
    }
}
