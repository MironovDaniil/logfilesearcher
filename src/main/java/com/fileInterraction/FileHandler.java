package com.fileInterraction;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class FileHandler {
    private File file;

    FileHandler(File file) {
        this.file = file;
    }

    List<Long> search(String searchedText) throws FileNotFoundException {
        String[] searchedLines = searchedText.split("\n");
        Scanner in = new Scanner(file);
        List<Long> result = new ArrayList<>();
        long counter = 1;
        while (in.hasNextLine()) {
            String line = in.nextLine();
            for (String searchedLine:searchedLines
                 ) {
                if (line.contains(searchedLine)) {
                    result.add(counter);
                    break;
                }
            }
            counter++;
        }
        in.close();
        return result;
    }
}
