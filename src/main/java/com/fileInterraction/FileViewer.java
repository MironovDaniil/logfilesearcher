package com.fileInterraction;

import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.Scanner;

public class FileViewer {
    private File file;
    private long linesCount;

    public FileViewer(@NotNull File file, long linesCount) {
        this.file = file;
        this.linesCount = linesCount;
    }

    public String viewString(long stringNumber, int stringsAroundBefore, int stringsAroundAfter) throws IOException {
        if (linesCount < stringsAroundAfter + stringNumber) {
            System.out.println(linesCount);
            return null;
        }
        if (stringNumber - stringsAroundBefore < 0) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        Scanner in = new Scanner(file);
        for (int i = 0; i < stringNumber - stringsAroundBefore - 1; i++) {
            in.nextLine();
        }
        for (int i = 0; i < (stringsAroundAfter + stringsAroundBefore) + 1; i++) {
            result.append(in.nextLine());
        }
        return result.toString();
    }
}
