package com.fileInterraction;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DirectoryHandlerTest {
    private DirectoryHandler directoryHandler;

    @Before
    public void setUp() {
        File directory = new File("C:\\search");
        directoryHandler = new DirectoryHandler(directory, "log");
    }

    @Test
    public void searchFiles_shouldFind_22_files_shouldPrintFilesFound() {
        Map<File, List<Long>> result = null;
        try {
            result = directoryHandler.searchFiles("сайт ежемесячно посещает несколько миллионов");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (File file : result.keySet()
        ) {
            System.out.println(file.getName());
        }
        assertEquals(22, result.size());
    }
}
