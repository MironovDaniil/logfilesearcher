package com.fileInterraction;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileHandlerTest {

    private FileHandler fileHandler;

    @Before
    public void setUp() {
        File file = new File("C:\\test.log");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        PrintWriter out = null;
        try {
            out = new PrintWriter(file.getAbsoluteFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        out.println("Мы разрабатываем сайт Dnevnik.ru - продукт для школ и организаций, позволяющий вести \n" +
                "образовательную деятельность в облачном решении. На данный момент Дневник используется в \n" +
                "десятках тысяч школ, сайт ежемесячно посещает несколько миллионов пользователей. \n" +
                "Мы ищем целеустремленного Бизнес-аналитика. Надеемся, что Вы станете частью нашей дружной команды");
        out.close();
        fileHandler = new FileHandler(file);
    }

    @Test
    public void search_shouldReturn2and3() throws IOException {
        String string = "образовательную деятельность в облачном решении. На данный момент Дневник используется в \n" +
                "десятках тысяч школ, сайт ежемесячно посещает несколько миллионов";
        List<Long> actual = fileHandler.search(string);
        List<Long> expected = new ArrayList<>(2);
        expected.add(2L);
        expected.add(3L);
        assertEquals(expected, actual);
    }
}
