package com.fileInterraction;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import static org.junit.Assert.assertEquals;

public class FileViewerTest {
    private FileViewer fileViewer;

    @Before
    public void setUp() {
        File file = new File("C:\\test.log");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        PrintWriter out = null;
        try {
            out = new PrintWriter(file.getAbsoluteFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        out.println("Мы разрабатываем сайт Dnevnik.ru - продукт для школ и организаций, позволяющий вести \n" +
                "образовательную деятельность в облачном решении. На данный момент Дневник используется в \n" +
                "десятках тысяч школ, сайт ежемесячно посещает несколько миллионов пользователей. \n" +
                "Мы ищем целеустремленного Бизнес-аналитика. Надеемся, что Вы станете частью нашей дружной команды");
        out.close();
        fileViewer = new FileViewer(file, 4);
    }

    @Test
    public void viewString_shouldReturnStrings_from2to4() {
        String expected = "";
        expected = expected + ("образовательную деятельность в облачном решении. На данный момент Дневник используется в ");
        expected = expected + ("десятках тысяч школ, сайт ежемесячно посещает несколько миллионов пользователей. ");
        expected = expected + ("Мы ищем целеустремленного Бизнес-аналитика. Надеемся, что Вы станете частью нашей дружной команды");
        String actual = "";
        try {
            actual = fileViewer.viewString(3, 1, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(expected, actual);
    }
}
