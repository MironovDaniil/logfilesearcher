package com.fileInterraction;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class IntegrationTests {
    private File directory;
    private String searchedText;

    @Before
    public void setUp() {
        directory = new File("C:\\search");
        searchedText = "сайт ежемесячно посещает несколько миллионов";
    }

    @Test
    public void integrationTest() {
        Map<File, List<Long>> result = null;
        DirectoryHandler directoryHandler = new DirectoryHandler(directory, "log");
        try {
            result = directoryHandler.searchFiles(searchedText);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("File was not found!");
            fail();
        }
        for (Map.Entry<File, List<Long>> entry : result.entrySet()
        ) {
            System.out.println("");
            System.out.println(entry.getKey().getName() + ":");
            for (long i : entry.getValue()
            ) {
                String strings = "";
                FileViewer fileViewer = new FileViewer(entry.getKey(), 5);
                try {
                    strings = fileViewer.viewString(3, 1, 1);
                    System.out.println("");
                    System.out.println(strings);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
